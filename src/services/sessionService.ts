import * as crypto from "crypto";

const sessions = new Map<string, any>();
const generateUUID = () => crypto.randomBytes(16).toString("hex");

export const SESSION_KEY = "session_id";

export const create = () => {
  const sessionId = generateUUID();
  sessions.set(sessionId, { visits: 1, firstVisit: new Date().toISOString() });

  return sessionId;
};

export const getUserstatistics = () => Array.from(sessions.values());

export const exists = (sessionId: string) => sessions.has(sessionId);

export const getUser = (sessionId: string) => sessions.get(sessionId);

export const incrementVisits = (sessionId: string) => {
  const userStatistics = sessions.get(sessionId);
  if (userStatistics) {
    userStatistics.visits++;
  }
};
