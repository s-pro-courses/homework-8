import * as Router from "koa-router";
import * as crypto from "crypto";
import {
  getUser,
  getUserstatistics,
  SESSION_KEY,
} from "../services/sessionService";

const router = new Router();

router.get("/", (ctx) => {
  ctx.body = "yes";
});

router.get("/statistics", (ctx) => {
  ctx.body = `${JSON.stringify(getUserstatistics())}`;
});

router.get("/statistics/me", (ctx) => {
  ctx.body = `${JSON.stringify(getUser(ctx.cookies.get(SESSION_KEY)))}`;
});

export default router;
