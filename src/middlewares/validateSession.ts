import {
  exists,
  incrementVisits,
  SESSION_KEY,
} from "../services/sessionService";

export const validateSession = (ctx, next) => {
  const sessionId = ctx.cookies.get(SESSION_KEY);
  console.log("validate : ", sessionId);
  if (!sessionId || (sessionId && exists(sessionId))) {
    incrementVisits(sessionId);
    return next();
  }

  ctx.body = "go out haker!)";
};
