import { create, SESSION_KEY } from "../services/sessionService";

export const createSession = (ctx, next) => {
  if (!ctx.cookies.get(SESSION_KEY)) {
    const newUserSession = create();
    console.log("new user :", newUserSession);
    ctx.cookies.set(SESSION_KEY, newUserSession, { httpOnly: true });
    return next();
  }

  next();
};
